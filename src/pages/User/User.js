import React from 'react';
import data from './user.json';
import Table from '../../components/elements/Table';

export default function User(){
    const columns=[
        'USERNAME',
        'EMAIL',
        'ACTION'
    ];
    return (
        <main>
            <div className="flex justify-center items-center h-screen" />
                <h1 className="text-red-600 text-3xl">USER</h1>
                <Table items={data} columns={columns}  />
        </main>
    );
}