import React from 'react';
import data from './product.json';
import Table from '../../components/elements/Table';

export default function Products(){
    const columns=[
        'PRODUCT NAME',
        'DESCRIPTION',
        'PRODUCT PRICE',
        'ACTION'
    ]
    return (
        <main>
            <div className="flex justify-center items-center h-screen" >
                <h1 className="text-red-600 text-3xl">PRODUCTS</h1>
                <Table  items={data} columns={columns} />
            </div>
        </main>
    );
}