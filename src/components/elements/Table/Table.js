import React from 'react';
import PropTypes from 'prop-types';

const titik = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };
  
export default function Table({items, columns}) {
 return (
  <table>
    <thead>
        <tr>
            {columns.map((i, idx) =>(
                <th className="p-2" key={idx}>{i}</th>
            ))}
        </tr>
    </thead>
    <tbody>
        {items.map((i, idx) => 
            i.username ? ( 
                // untuk User
                <tr key={idx} className="border-b">
                    <td>{i.username}</td>
                    <td>{i.email}</td>
                    <td className="p-2">
                        <button className="py-2 px-2 bg-red-500 hover:bg-red-700 text-black font-bold rounded">Delete</button>
                    </td>

                    <td />
                </tr>
            ) : (
                // untuk Products
                <tr key={idx} className="border-b">
                    <td>{i.name}</td>
                    <td>{i.description}</td>
                    <td >Rp{titik(i.price)}</td>
                    <td className="p-2">
                        <button className="py-2 px-2 bg-red-500 hover:bg-red-700 text-black font-bold rounded">Delete</button>
                    </td>
                    <td />
                </tr>
            )
        )}    
    </tbody>
  </table>
  );
}
Table.propTypes = {
    columns: PropTypes.array.isRequired,
    items: PropTypes.array.isRequired,
};
  
